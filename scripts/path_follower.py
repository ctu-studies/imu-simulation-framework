#!/usr/bin/env python

import utils
import ConfigParser
import sys
import os
import pandas as pd 
import math
import numpy as np
import time
import traceback
import logging

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import rospy
import signal
from std_msgs.msg import Float64
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState, SetModelStateRequest
from std_srvs.srv import SetBool, SetBoolRequest

from imu_simulation_framework.msg import ErrorCoordinates2
from imu_simulation_framework.srv import StartStopTime, StartStopTimeRequest


class PathFollower:
    """
    ROS node publishing commands for wheel_velocity_controller in Gazebo to make 
    the differential drive robot approximate the trajectory provided as a set of waypoints in CSV file. 
    This node should start as the first one from the whole IMU simulation framework.

    2018 - 2019, David Cesenek, cesendav@fel.cvut.cz

    """
    ########################################################################################
    # init
    def main(self, ini_file):
            
        if self.load_parameters(ini_file) is True: 
            try:
                time_suffix = time.strftime("%Y%m%d-%H%M%S")
                utils.save_config_file(time_suffix, ini_file, self.output_dir)

                # if there are result_comparison of error_publisher scripts already running,
                # kill them before starting new experiment
                output1 = os.popen('rosnode list | grep result_comparison').read()
                output2 = os.popen('rosnode list | grep error_publisher').read()
                
                if 'result_comparison' in output1 :
                    rospy.logerr("Running result_comparison script detected, killing it!")
                    pid = os.popen('ps aux | grep result_comparison | grep -v grep | awk \'{print $2}\' ').read()
                    os.popen('kill -signum '+pid)
                elif 'error_publisher' in output2 :
                    rospy.logerr("Running error_publisher script detected, killing it!")
                    pid = os.popen('ps aux | grep error_publisher | grep -v grep | awk \'{print $2}\' ').read()
                    os.popen('kill -signum '+pid)
                
                t, dt, x, y, dx, dy, vx, vy, yaw, vyaw = self.load_reference_trajctory_data()
                rospy.loginfo("Raw trajectory data loaded.")
                               
                velocity_right, velocity_left = self.compute_wheel_velocities_inverse_kinematic_model(vx, vy, yaw, vyaw)
                rospy.loginfo("The wheel velocities computed by inverse kinematics model!")
                # firstly, store the data in a temporal copy for result_comparison and error_publisher scripts,
                #  optionally, save it also for future usage:
                file_name = self.output_dir+'trajectory_data.csv'
                utils.save_trajectory_data(t, x, y, yaw, velocity_left, velocity_right,file_name)
                file_name = self.output_dir + time_suffix + '_trajectory_data.csv'                 
                utils.save_trajectory_data(t, x, y, yaw, velocity_left, velocity_right,file_name) 
                rospy.loginfo("Time stamp of this experiment: %s", time_suffix)
                self.send_start_stop_time_srv(time_suffix)
                
                self.publisher(dt, x, y, yaw, velocity_left, velocity_right, time_suffix)    
                
            
            except (IOError, TypeError, NameError ) as e:
                rospy.logerr("Error in the path_follower main():, %s", e)                
            
        else:
            rospy.logerr("There was an error when loading the confing from the file: %s",ini_file)

        rospy.loginfo("Path_follower script finished!")

    def __init__(self,ini_file):
        
        rospy.init_node('path_follower', anonymous=False) 
        rospy.loginfo("Path_follower script started!")
        
        # init all instance variables:
        self.output_dir = 'simulation_log/'
        self.E_dist = 0
        self.E_yaw = 0
        self.yaw_error = 0
        self.x_error = 0
        self.y_error = 0
        self.dist_error = 0
        self.dist_theta = 0 # dist_theta = yaw_ref - "angle of the vector: (x_ref - x, y_ref - y)", shows, in which direction the distance correction should be done
        self.yaw_error_old = 0
        self.dist_error_old = 0
        self.array_size = 1000000
        self.error_debug_data = np.zeros((self.array_size,3)) # [time, yaw_error, dist_error]
        self.control_debug_data = np.zeros((self.array_size,9)) # [time, wheel_vel_l_final, wheel_vel_r_final, u_yaw, wheel_vel_yaw_l, wheel_vel_yaw_r, u_dist, wheel_vel_dist_l, wheel_vel_dist_r]
        self.error_debug_data_idx = 0
        self.control_debug_data_idx = 0
        self.start_rostime = rospy.Time(0, 0)
        self.stop_rostime = rospy.Time(0, 0)
        self.publisher_right = None
        self.publisher_left = None
        
        self.R = 0.126
        self.L = 0.23025
        self.stop_idx = 0
        self.start_idx = 0
        self.interpolate_trajectory_data = True
        self.time_step = 0.15
        self.normalize = True
        self.mock_data = False
        self.use_feedback_control = True
        self.x_offset = 0.0
        self.debug = True
        self.threshold_yaw = 0.02
        self.Kp_yaw = 1.0
        self.Ki_yaw = 0.01
        self.Kd_yaw = 0.0
            
        self.threshold_dist = 0.03
        self.Kp_dist = 0.0
        self.Ki_dist = 0.0
        self.Kd_dist = 0.0

        self.threshold_theta = 0.15
        self.threshold_dist_accel= 0.1

        self.trajectory_raw_data_csv_file = 'trajectory.csv'
        self.rear_wheel_velocity_left_topic = '/command_left'
        self.rear_wheel_velocity_right_topic = '/command_right'
        
        self.main(ini_file)

    def init_ros(self):  
        try:    
            self.publisher_right = rospy.Publisher(self.rear_wheel_velocity_right_topic, Float64, queue_size=10)
            self.publisher_left = rospy.Publisher(self.rear_wheel_velocity_left_topic, Float64, queue_size=10)
            rospy.Subscriber('/error_coordinates2', ErrorCoordinates2, self.error_coordinates_callback)
            # rospy.Rate(update_rate) 
        
        except rospy.ROSException as e:
            rospy.logerr("ROS init error: %s", e)
    
    def load_parameters(self, ini_file):
        """Load the parameters from provide ini_file."""

        config = ConfigParser.SafeConfigParser()
        if os.path.isfile(ini_file):
            config.read(ini_file)
        else:
            rospy.logerr("Error, the config file: %s not found!", ini_file)
            return False        

        try:
            rospy.loginfo('Loading the parameters from config file:%s \n--------------------------------------------------')
            self.R = config.getfloat('path_follower','R')
            rospy.loginfo("R set to: %f", self.R)
            self.L = config.getfloat('path_follower','L') 
            rospy.loginfo("L set to: %f", self.L)
            self.stop_idx = config.getint('path_follower','stop_idx')
            rospy.loginfo("stop_idx set to %d:",self.stop_idx)
            self.start_idx = config.getint('path_follower','start_idx')
            rospy.loginfo("start_idx set to %d:", self.start_idx)
            self.interpolate_trajectory_data = config.getboolean('path_follower','interpolate_trajectory_data')
            rospy.loginfo("interpolate_trajectory_data set to:%d",self.interpolate_trajectory_data)
            self.time_step = config.getfloat('path_follower', 'time_step')
            rospy.loginfo("time_step set to:%f",self.time_step)
            self.normalize = config.getboolean('path_follower','normalize')
            rospy.loginfo("normalize set to:%d",self.normalize)
            self.mock_data = config.getboolean('path_follower', 'mock_data')
            rospy.loginfo("mock_data set to:%d",self.mock_data)
            self.use_feedback_control = config.getboolean('path_follower', 'use_feedback_control')
            rospy.loginfo("use_feedback control set to:%d",self.use_feedback_control)
            self.debug = config.getboolean('path_follower', 'debug')
            rospy.loginfo("debug set to:%d",self.debug)
            self.threshold_yaw = config.getfloat('path_follower', 'threshold_yaw')
            rospy.loginfo("threshold_yaw set to:%f",self.threshold_yaw)
            self.x_offset = config.getfloat('path_follower','x_offset')
            rospy.loginfo("x_offset set to:%f",self.x_offset)
            self.Kp_yaw = config.getfloat('path_follower', 'Kp_yaw')
            rospy.loginfo("Kp_yaw set to:%f",self.Kp_yaw)
            self.Ki_yaw = config.getfloat('path_follower', 'Ki_yaw')
            rospy.loginfo("Ki_yaw set to:%f",self.Ki_yaw)
            self.Kd_yaw = config.getfloat('path_follower', 'Kd_yaw')
            rospy.loginfo("Kd_yaw set to:%f",self.Kd_yaw)
            
            self.threshold_dist = config.getfloat('path_follower', 'threshold_dist')
            rospy.loginfo("threshold_dist set to:%f",self.threshold_dist)
            self.Kp_dist = config.getfloat('path_follower', 'Kp_dist')
            rospy.loginfo("Kp_dist set to:%f",self.Kp_dist)
            self.Ki_dist = config.getfloat('path_follower', 'Ki_dist')
            rospy.loginfo("Ki_dist set to:%f",self.Ki_dist)
            self.Kd_dist = config.getfloat('path_follower', 'Kd_dist')
            rospy.loginfo("Ki_dist set to:%f",self.Ki_dist)

            self.threshold_theta= config.getfloat('path_follower', 'threshold_theta')
            rospy.loginfo("threshold_theta set to:%f",self.threshold_theta)
            self.threshold_dist_accel= config.getfloat('path_follower', 'threshold_dist_accel')
            rospy.loginfo("threshold_dist_accel set to:%f",self.threshold_dist_accel)

            self.output_dir = config.get('common','output_dir')
            rospy.loginfo("output_dir set to:%s",self.output_dir)
            
            self.trajectory_raw_data_csv_file = config.get('common','trajectory_raw_data_csv_file')
            rospy.loginfo("trajectory_raw_data_csv_file set to:%s",self.trajectory_raw_data_csv_file)
            self.rear_wheel_velocity_left_topic = config.get('common', 'rear_wheel_velocity_left_topic')
            rospy.loginfo("rear_wheel_velocity_left_topic set to:%s",self.rear_wheel_velocity_left_topic)
            self.rear_wheel_velocity_right_topic = config.get('common', 'rear_wheel_velocity_right_topic')
            rospy.loginfo("rear_wheel_velocity_right_topic set to:%s",self.rear_wheel_velocity_right_topic)
            rospy.loginfo('--------------------------------------------------\n')
        except ConfigParser.NoSectionError as e:
            rospy.logerr("NoSectionError when parsing the config file: %s", ini_file)
            rospy.logerr(e)
            return False
        except ConfigParser.NoOptionError as e:
            rospy.logerr("NoOptionError  when parsing the config file: %s", ini_file)
            rospy.logerr(e)
            return False

        return True

    def find_rostime_limits(self, rostime_secs, rostime_nsecs, time_normalised, start_idx, stop_idx):
        """
        To load the IMU data in result_comparison script correctly (only for the corresponding time window),
        find the the start and stop experiment rostime and take into account the applied data interpolation.
        """
        start_rostime = rospy.Time(rostime_secs[0], rostime_nsecs[0])
        rospy.loginfo("raw data starts at rostime: %i sec",start_rostime.secs)
        time_offset_start = time_normalised[start_idx]
        
        start_rostime = start_rostime + rospy.Duration.from_sec(time_offset_start)
        
        stop_rostime = start_rostime + rospy.Duration.from_sec(time_normalised[stop_idx] - time_normalised[start_idx])
        
        rospy.loginfo("Detected stop rostime:nsecs:%f",stop_rostime.secs)
        rospy.loginfo("Detected start rostime:secs:%f",start_rostime.secs)
        rospy.loginfo("Detected raw data experiment duration: %i sec", (stop_rostime - start_rostime).secs)
        
        return start_rostime, stop_rostime

    def load_reference_trajctory_data(self):

        if self.mock_data is False:
            file_path = os.path.join(os.path.abspath(os.path.curdir), self.trajectory_raw_data_csv_file)
            
            if not os.path.isfile(file_path):
                rospy.logerr("The file with raw data: %s does not exist!", file_path)
                raise IOError

            rospy.loginfo("Loading RAW data with reference positions from the file:\n ---> %s", file_path)
            df = pd.read_csv(self.trajectory_raw_data_csv_file, delimiter=';')
            time = utils.get_time_in_nanoseconds(df['secs'].values, df['nsecs'].values) # time axis in nsec     
            quaternions = np.array([df['x.1'].values, df['y.1'].values, df['z.1'].values, df['w'].values ])
            yaw = utils.quaternions_to_yaw(quaternions,'sxyz')
            x = df['x'].values
            y = df['y'].values

            time = utils.get_time_in_seconds(utils.normalize_array(time)) # normalize time always (to start at 0sec)
            yaw = utils.convert_yaw_format(yaw) # convert the angle to the notation: <0, 2*pi>
        else:
            time, x, y, yaw = utils.generate_mock_data()

        if self.normalize: # should robot start also at position [0,0] ?
            x = utils.normalize_array(x)
            y = utils.normalize_array(y)    
        
        if self.interpolate_trajectory_data:
            time, x, y, yaw = utils.interpolate_data(time, x, y, yaw, interpolation_type='linear', time_step=self.time_step)

        # compute relative increments (differences) of time and position
        dt = np.roll(time, -1, axis=0) - time  # time [nseconds]
        dx = np.roll(x, -1, axis=0) - x  # x axis [meters]
        dy = np.roll(y, -1, axis=0) - y  # y axis [meters]
        
        # for yaw angle, the relative increments must take into account the transition between angle slightly
        # grater than 0 and angle close to 2*pi:
        dyaw = np.zeros(np.shape(yaw)) 
        yaw_shifted = np.roll(yaw, -1, axis=0)

        for i, (th, th_sh) in enumerate(zip(yaw, yaw_shifted)):
            # 3 is an adhock value that works well (yaw change in one step grater than 3 rad(almost 180deg) doesn't make sense)
            if ((th_sh - th) < -3):    # 2pi >> 0
                dyaw[i] = (2*math.pi - th) + th_sh
            elif ((th_sh - th) > 3): # 0 >> 2pi
                dyaw[i] = -((2*math.pi -th_sh) + th) #sign - represents decrement in anle
            else:
                dyaw[i] = th_sh - th 
        
        # instantanous velocity increments
        vx = dx/dt
        vy = dy/dt
        vyaw = dyaw/dt

        # remove the last value since it doesn't make sense due to value shif
        dt = np.delete(dt,-1)
        x = np.delete(x,-1)
        y = np.delete(y,-1)
        vx = np.delete(vx,-1)
        vy = np.delete(vy,-1)
        yaw = np.delete(yaw,-1)
        vyaw = np.delete(vyaw,-1)
        
        if self.mock_data is True: # ignore start and stop indexes
            self.start_idx = 0
            self.stop_idx = np.size(time) -1
        else:
            # if the indexed are not correct, use the full possible range
            if self.start_idx < 0:
                self.start_idx = 0
            if self.stop_idx < 0 or self.stop_idx > np.size(time):
                self.stop_idx = np.size(time) -1
            
            rospy.loginfo("Start index set to: %d", self.start_idx)
            rospy.loginfo("Stop index set to: %d", self.stop_idx)
            
            self.start_rostime, self.stop_rostime = self.find_rostime_limits(df['secs'].values, df['nsecs'].values, time, self.start_idx, self.stop_idx)
            
        return utils.normalize_array(time[self.start_idx:self.stop_idx]), dt[self.start_idx:self.stop_idx],\
            x[self.start_idx:self.stop_idx], y[self.start_idx:self.stop_idx],\
            dx[self.start_idx:self.stop_idx], dy[self.start_idx:self.stop_idx],\
            vx[self.start_idx:self.stop_idx], vy[self.start_idx:self.stop_idx], yaw[self.start_idx:self.stop_idx], vyaw[self.start_idx:self.stop_idx]  

    ########################################################################################
    #  ROS callbacks, services

    def error_coordinates_callback(self, msg):
        
        self.x_error = msg.x_error
        self.y_error = msg.y_error
        self.dist_error = np.sqrt(msg.x_error**2 + msg.y_error**2) # Euclidian distance between the desired and real positionmsg.l2_distance_error
        self.yaw_error = msg.yaw_error
        self.dist_theta = msg.dist_theta

    def set_init_robot_pose_srv(self, x, y, yaw):
        """
        Place the robot model to the desired initial 2D position.
        """
        quaternions = utils.euler_to_quaternions(0,0,yaw)
        sms_rq = SetModelStateRequest()
        
        # there is a offset between the origin of the middlepoint between rear wheels and the world_frame origin
        sms_rq.model_state.pose.position.x = x + self.x_offset*np.cos(yaw)
        sms_rq.model_state.pose.position.y = y + self.x_offset*np.sin(yaw)
        
        sms_rq.model_state.pose.orientation.x = quaternions[0]
        sms_rq.model_state.pose.orientation.y = quaternions[1]
        sms_rq.model_state.pose.orientation.z = quaternions[2]
        sms_rq.model_state.pose.orientation.w = quaternions[3]
        sms_rq.model_state.model_name = "robot"

        rospy.wait_for_service('/gazebo/set_model_state')
        try:
            set_model_position = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
            set_model_position(sms_rq)
        except rospy.ServiceException as e:
            print("Service call failed: %s", e)
    
    def send_start_stop_time_srv(self, time_suffix):
        rospy.loginfo("Waiting for result_comparison script!")
        rospy.wait_for_service('start_stop_time')
        try:
            sst_rq = StartStopTimeRequest()
            sst_rq.start_rostime = self.start_rostime
            sst_rq.stop_rostime = self.stop_rostime
            sst_rq.time_suffix = time_suffix 
            start_stop_srv = rospy.ServiceProxy('/start_stop_time', StartStopTime)
            response = start_stop_srv(sst_rq)
            return response.result
        except(rospy.ServiceException, rospy.ROSException, ROSInterruptException) as e:
            rospy.logerr(e)
            return False

    def collect_data_srv(self, command):
        """
        Trigger or stop the data capture performed by "result_comparison" script.
        """
        rospy.loginfo("Waiting for result_comparison script!")
        rospy.wait_for_service('collect_data')
        rospy.loginfo("Trying to send command to start/stop data collection in result_comparison script: %d", command)
        try:
            cd_srv = rospy.ServiceProxy('collect_data', SetBool)
            response = cd_srv(command)
            return response.success
        
        except (rospy.ServiceException, rospy.ROSException) as e:
            if command is True: # When closing the result_comparison script, there is no correct response
                rospy.logerr("Service call failed: %s", e)
            else:
                return True
 
    ########################################################################################       
    # Approximate the trajectory fro the raw data 

    def compute_wheel_velocities_inverse_kinematic_model(self, vx, vy, yaw, vyaw):
        """
        Compute wheel velocities using inverse kinematics model provided by Mr. Stockburger
        """   
        velocity_right = ( self.L*vyaw + np.sin(yaw)*vy + np.cos(yaw)*vx)/(self.R)
        velocity_left  = (-self.L*vyaw + np.sin(yaw)*vy + np.cos(yaw)*vx)/(self.R)

        return velocity_right, velocity_left

    def compute_velocities_from_wheel_velocities(self, yaw, velocity_right, velocity_left):
        """ Compute back the robot's velocity defferencies from the obtained wheel velocities using direct kinematics model. Useful for inverse kinematics debugging. """
        vx = (self.R/2)*np.cos(yaw) * (velocity_left + velocity_right)
        vy = (self.R/2)*np.sin(yaw) * (velocity_left + velocity_right)
        vyaw = (self.R/(2*self.L)) * (velocity_right - velocity_left)

        return vx, vy, vyaw
    
    def control_wheel_velocities(self, time, velocity_left, velocity_right, debug=True):        

        # if the error could not be computed or is negligible, do not change the velocities
        # if the error cannot be computed, the yaw_error ==-1000, dist_error > 1000
        if (self.yaw_error < -100 or self.x_error < -100 or self.y_error < -100 or self.dist_theta < -100) or (self.yaw_error == 0 and self.dist_error == 0): 
            return velocity_left, velocity_right

        if debug is True:
                new_row = np.array([[time, self.yaw_error, self.dist_error]], dtype=Float64)
                self.error_debug_data[self.error_debug_data_idx, :] = new_row
                self.error_debug_data_idx += 1
            
        
        ##################################################################
        # YAW controller
        velocity_left_yaw, velocity_right_yaw, u_yaw = 0.0, 0.0, 0.0
        if np.abs(self.yaw_error) > self.threshold_yaw:   
            
            self.E_yaw = self.E_yaw + self.yaw_error # integral approx.
            ed_yaw = self.yaw_error - self.yaw_error_old # derivation approx.
            self.yaw_error_old = self.yaw_error
            u_yaw = np.abs(self.yaw_error * self.Kp_yaw + self.E_yaw * self.Ki_yaw + ed_yaw * self.Kd_yaw )
        
            if (self.yaw_error < 0):  # turn right
                velocity_left_yaw = velocity_left + u_yaw
                velocity_right_yaw = velocity_right - u_yaw                     
            else:                     # turn left
                velocity_left_yaw = velocity_left - u_yaw
                velocity_right_yaw = velocity_right + u_yaw 
        else:
            velocity_left_yaw = velocity_left
            velocity_right_yaw = velocity_right
        
        #################################################################
        # Distance control
        velocity_left_final, velocity_right_final, u_dist = 0.0, 0.0, 0.0
        if self.dist_error > self.threshold_dist: 

            self.E_dist = self.E_dist + self.dist_error # integral approx.
            ed_dist = self.dist_error - self.dist_error_old # derivation approx.
            self.dist_error_old = self.dist_error
            u_dist = np.abs(self.dist_error * self.Kp_dist + self.E_dist * self.Ki_dist + ed_dist * self.Kd_dist)     
            
            if self.threshold_dist_accel > self.dist_theta > -self.threshold_dist_accel: # keep the direction, accelerate the robot only
                velocity_left_final = velocity_left_yaw + self.dist_error*1.1
                velocity_right_final = velocity_right_yaw + self.dist_error*1.1
                
                if debug is True:
                    rospy.loginfo("Dist PID: accel, vel_l: %f, vel_r: %f", velocity_left_final, velocity_right_final)

            # feedback L2 distance control
            elif self.dist_theta > self.threshold_dist_accel:  # turn right         
                velocity_left_final = velocity_left_yaw + u_dist
                velocity_right_final = velocity_right_yaw - u_dist   
                if debug is True:
                    rospy.loginfo("Dist pid: R: yaw_error:%f,dist_error:%f, vel_r:%f, vel_l:%f",self.yaw_error, self.dist_error, velocity_left_final, velocity_right_final)
                
            else: # turn left
                velocity_left_final = velocity_left_yaw - u_dist
                velocity_right_final = velocity_right_yaw + u_dist
                
                if debug is True:
                    rospy.loginfo("Dist PID: L: yaw_error:%f,dist_error:%f, vel_r:%f, vel_l:%f",self.yaw_error, self.dist_error, velocity_left_final, velocity_right_final)
        else:
            velocity_left_final = velocity_left_yaw
            velocity_right_final = velocity_right_yaw
        
        # it the corrected wheel velocities exceed by 50% the precomputed values, don't correct it at all and use the original values (to avoid wired behavior)
        if velocity_left_final > velocity_left * 2 and velocity_right_final > velocity_right * 2:
            rospy.loginfo("Too high correction, control correction ommited")
            velocity_left_final = velocity_left
            velocity_right_final = velocity_right
        
        if debug:
            new_row = np.array([[time, velocity_left, velocity_right,\
                u_yaw, velocity_left_yaw, velocity_right_yaw,\
                u_dist, velocity_left_final, velocity_right_final,  ]])
            self.control_debug_data[self.control_debug_data_idx, :] = new_row
            self.control_debug_data_idx += 1

        return velocity_left_final, velocity_right_final

    def plot_control_error(self, time_suffix):
        """ plot the control PID controller output - the regulator correction """

        time = utils.normalize_array(self.error_debug_data[:self.error_debug_data_idx,0])
        plt.figure('Error data')
        plt.subplots_adjust(hspace = 0.5)
        plt.subplot(211)
        plt.grid()
        plt.title('Yaw error')
        plt.xlabel('Time [secs]')
        plt.ylabel('Error [rad]')
        plt.plot(time, np.abs(self.error_debug_data[:self.error_debug_data_idx,1]))
        
        plt.subplot(212)
        plt.title('Distance error')
        plt.xlabel('Time [secs]')
        plt.ylabel('Error [m]')
        plt.grid()
        plt.plot(time, self.error_debug_data[:self.error_debug_data_idx,2])
        
        plt.savefig(self.output_dir+time_suffix +'_control_error.png')
        plt.savefig(self.output_dir+time_suffix +'_control_error.eps')
        
    def publisher(self, dt, x, y, yaw, velocity_left, velocity_right, time_suffix):
        """
        The main update loop, sends periodicaly the updated to the robot.
        """
        self.init_ros()        
        t1 = rospy.Time(0, 0)
        
        self.collect_data_srv(True) # trigger collecting data in result_comparison script
        self.set_init_robot_pose_srv(x[0], y[0], yaw[0]) 
        rospy.loginfo("Starting the control loop!")
        
        i = 0 # iterations counter
        while (not rospy.is_shutdown()) and (i < (self.stop_idx - self.start_idx)):         
            t1 = rospy.get_rostime() # returns 0, when /clock is not published!               
            
            if self.use_feedback_control is True:
                try: 
                    vel_l, vel_r = self.control_wheel_velocities(t1.to_sec(), velocity_left[i], velocity_right[i], debug=self.debug)
                except IndexError:
                    rospy.logerr("The debug numpy arrays excedded the internal limit, stoping the trajectory approximation!")
                    continue
            else:
                vel_l = velocity_left[i]
                vel_r = velocity_right[i]
                if self.debug is True:
                    rospy.loginfo('vel_l:%.3f, vel_r:%.3f', vel_l, vel_r)

            self.publisher_left.publish(vel_l)
            self.publisher_right.publish(vel_r)        
                
            # correct the waiting time caused by control computation and output
            dt_code_exec = rospy.get_rostime() - t1 
            rospy.sleep(dt[i] - dt_code_exec.to_sec())       
            i = i+1
        
        # stop the robot in the end of experiment
        for i in range(100):
            self.publisher_left.publish(0)
            self.publisher_right.publish(0)    
        rospy.loginfo("Control loop stoped.")    
       
        try:
            self.collect_data_srv(False) # stop result comparison script
        except Exception as e:
            rospy.logerr(traceback.format_exc())
            print(e)
        # DEBUG:
        if self.debug is True:
            np.savetxt(self.output_dir+time_suffix+'_error_debug_data.csv', self.error_debug_data[:self.error_debug_data_idx,:], delimiter=';', header='time, yaw_error, distance_error')
            np.savetxt(self.output_dir+time_suffix+'_control_debug_data.csv', self.control_debug_data[:self.control_debug_data_idx,:], delimiter=';', \
                header='time, velocity_left, velocity_right, u_yaw, velocity_left_yaw, velocity_right_yaw, u_dist, velocity_left_final, velocity_right_final')
            # np.savetxt(self.output_dir'+time_suffix+'_pid_parameters.csv',[[self.Kp_yaw, self.Ki_yaw, Kd_yaw, Kp_dist, Ki_dist, Kd_dist, threshold_dist, threshold_yaw]], delimiter=';', header='Kp_yaw, Ki_yaw, Kd_yaw, Kp_dist, Ki_dist, Kd_dist, threshold_dist, threshold_yaw')
        self.plot_control_error(time_suffix)
       
        
   ########################################################################################        
if __name__ == '__main__':
    try:
        if len(sys.argv) > 1:
            pf = PathFollower(sys.argv[1])
        else:
            raise ValueError
        
    except ValueError:
        print("Path follower error: no config file given!")
    except AssertionError:
        print("Path follower assertion error!")
    except rospy.ROSInterruptException as e:
        rospy.logerr("ROS interrupt: %s", e)
        
            
