#!/usr/bin/env python

import utils
import time
import signal 
import sys
import os
import numpy as np
import pandas as pd
import ConfigParser
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.signal import medfilt
import rospy
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
from control_msgs.msg import JointControllerState
from imu_simulation_framework.srv import StartStopTime, StartStopTimeResponse
from std_srvs.srv import SetBool, SetBoolResponse
from gazebo_msgs.msg import LinkStates
from nav_msgs.msg import Odometry
import dynamic_reconfigure.client

"""

ROS node performing the comparison the simulated IMU output with the dataset from real IMU sensor.
It subscribes to set ground truth position, wheel velocities, and IMU topics and communicates with path_follower script 
via custom ROS services.
Part of the imu_simulation_framework ROS package, it should start after the path_follower and error_publisher scripts.

2018-2019, David Cesenek, cesendav@fel.cvut.cz
"""



##############################################################################
# GLOBAL VARIABLES

# global variable to enable catching SIGTERM/ SIGINT signal and save quit
kill_now = False
collect_data = False
imu_raw_data_csv_file='raw_data/_slash_imu_leo_6dof.csv'

# in nanoseconds, the timestamp when the raw data capturing started
raw_data_start_rostime = rospy.Time(0,0)
raw_data_stop_rostime =  rospy.Time(0,0)
time_suffix = ''
output_dir = '/home/user/simulation_log/'
start_time = 0
stop_time = 0

# convention: time is stored in seconds
# position of robot, 4 columns: time, x, y, yaw
dtype = np.float64 # the data representation used to store the numbers
array_size = 100000 # the maximal number of array items, the capture script ends if this value is exceeded

simulated_position = np.zeros((array_size,4),dtype=dtype)
real_position = np.zeros((array_size,4), dtype=dtype)
simulated_wheel_velocity = np.zeros((array_size,3), dtype=dtype)
simulated_imu = np.zeros((array_size,11), dtype=dtype) # imu output: 11 columns: time, lin_accel_x, lin_accel_y,lin_accel_z, ang_vel_x,ang_vel_y,ang_vel_z
joint_rear_right = np.zeros((array_size,2), dtype=dtype)
joint_rear_left = np.zeros((array_size,2), dtype=dtype)

# indexs pointing to the current position in the array
simulated_position_idx = 0
simulated_imu_idx = 0
simulated_wheel_velocity_idx = 0
joint_rear_right_idx = 0
joint_rear_left_idx = 0
 
real_wheel_velocities = np.zeros((0,3)) # time, velocity_left, velocity_right
real_imu = np.ndarray((0,11), dtype=dtype)
plot_trajectory = True
plot_wheel_velocities = False
plot_wheel_velocities_diff = False
plot_imu_gt_yaw = True
plot_imu_gt_yaw_error = True
plot_psd = True
filter_peaks=True
median_filter_window_size = 519
time_shift=True

real_imu_label= 'real BNO055'
imu_topic='/imu'
joint_states_topic='/joint_states'
gt_topic = '/gt'

wheel_velocity_command_topic = 'wheel_velocity_command'
rear_wheel_velocity_left_topic = 'wheel_velocity_left'
rear_wheel_velocity_right_topic = 'wheel_velocity_right'

hector_bias = 0.0
use_hector_imu = False
drift_std = 0.0
drift_frequency = 0.0
white_noise_std = 0.0
##############################################################################


def plot_trajectories(x_ref, y_ref, x_sim, y_sim):
    """
    Plots the comparison of desired and truly realized trajectory in Gazebo.
    """
    plt.figure('trajectory difference',figsize=(8, 6))
    plt.title('Real vs. simulated robot position') 
    plt.plot(x_ref, y_ref, label='reference position')
    plt.plot(x_sim, y_sim, label='simulated position')
    plt.grid()
    plt.legend(loc='lower right')
    plt.axis('equal')
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.savefig(output_dir+time_suffix + '_trajectory.png', dpi=300)
    rospy.loginfo('Plotted trajectories saved in:\n ---> ' + output_dir+time_suffix + '_trajectory.png')
    plt.clf()
       
def plot_wheel_velocities(time_ref, time_sim, vel_l_ref, vel_r_ref, vel_l_sim, vel_r_sim):
    """
    Plots simulated and desired wheel velocities, experimental.
    """    
    plt.ion()
    plt.figure(2)
    plt.title('Instantaneous left wheel velocity')
    plt.plot(time_ref, vel_l_ref, label='reference L')
    plt.plot(time_sim, vel_l_sim, label='simulation L')
    plt.xlabel('Time [sec]')    
    plt.ylabel('Wheel velocity [rad/s]')
    plt.legend(loc='lower right')
    plt.grid()
    plt.savefig(output_dir+time_suffix + '_left_wheel_velocities.png', dpi=300)

    plt.figure(3)
    plt.title('Instantaneous right wheel velocity')
    plt.plot(time_ref, vel_r_ref, label='reference')
    plt.plot(time_sim, vel_r_sim, label='simulation')
    plt.xlabel('Time [sec]')    
    plt.ylabel('Wheel velocity [rad/s]')
    plt.legend(loc='lower right')
    plt.grid()
    
    plt.savefig(output_dir+time_suffix + '_right_wheel_velocities.png', dpi=300)
    plt.show()

def plot_velocity_difference(time_sim, time_ref, vel_l_ref, vel_r_ref, vel_l_sim, vel_r_sim):
    """
    Plots the difference between desired and truly performed wheel velocities, experimental.
    """
    time, difference_l = utils.compute_1l_difference(time_ref, time_sim, vel_l_ref, vel_l_sim)
    time, difference_r = utils.compute_1l_difference(time_ref, time_sim, vel_r_ref, vel_r_sim)

    plt.figure('Wheel velocity difference',figsize=(8, 6))
    plt.title('Wheel velocity difference')
    plt.plot(time, difference_l, label='left wheel velocity difference', color='b')
    plt.plot(time, difference_r, label='right wheel velocity difference', color='g')
    plt.xlabel('time [sec]')    
    plt.ylabel('Wheel velocity difference [rad/s]')
    plt.legend(loc='lower right')
    plt.grid()
    plt.savefig(output_dir+time_suffix + '_wheel_velocities_difference.png', dpi=300)
    plt.show()

def plot_trajectory_difference(time_ref, time_sim, x_ref, y_ref, yaw_ref, x_sim, y_sim, yaw_sim):
    """
    Plots the difference between simulated and desired trejectories, experimental.
    """
    time, yaw_diff = utils.compute_1l_difference(time_ref, time_sim, yaw_ref, yaw_sim)
    plt.subplot(212)
    plt.title('Orientation (yaw) difference')
    plt.plot(time, yaw_diff, label='yaw difference')
    plt.xlabel('Time [sec]')    
    plt.ylabel('Angle [rad]')
    plt.legend(loc='lower right')
    plt.grid()
    plt.savefig(output_dir+time_suffix+ 'yaw_difference.png', dpi=300)
    plt.show()
      
    position_difference = utils.compute_2l_difference(time_ref, time_sim, x_ref, y_ref, x_sim, y_sim)
    plt.figure('Trajectory difference',figsize=(8, 6))
    plt.subplot(211)
    plt.title('Position difference')
    plt.plot(time_ref, position_difference, label='position difference (L2 norm)')
    plt.xlabel('Time [sec]')    
    plt.ylabel('Difference [m]')
    plt.legend(loc='lower right')
    plt.grid()
    plt.savefig(output_dir+time_suffix+ 'position_difference.png', dpi=300)

def find_experiment_boundaries():
    global start_time, stop_time
    
    if (start_time == 0 and stop_time == 0):
        rospy.logerr("the wheel velocities were not captured!")
        sys.exit(-1)
    rospy.loginfo("ROS time: %f detected as experiment stop time", stop_time)
    
    try:
        velocity_start_index = utils.find_nearest_index(simulated_wheel_velocity[:,0], value=start_time)
        velocity_stop_index = utils.find_nearest_index(simulated_wheel_velocity[:,0], value=stop_time)
        
    except ValueError:
        rospy.loginfo("The wheel velocities were not captured!")
        return None, None, None, None, None
    try: 
        position_start_index = utils.find_nearest_index(simulated_position[:,0], value=start_time)
    except ValueError:
        rospy.loginfo("The wheel velocities were not captured!")
        return start_time, stop_time, velocity_start_index, velocity_start_index, None

    return velocity_start_index, velocity_stop_index, position_start_index

def plot_yaw_angle(time_sim_gt, time_sim_imu, time_real_gt, time_real_imu,
                   yaw_sim_gt, yaw_sim_imu, yaw_real_gt, yaw_real_imu, external=False, time_shift=False
                   ):
    """
    Plots the yaw angle outputs (reference yaw (GPS), real IMU yaw, simulated groud truth, simulated IMU yaw).
    """
    if time_shift is True:
        shift_real = utils.compute_2_signals_shift(time_real_gt, time_real_imu, yaw_real_gt, yaw_real_imu)
        rospy.loginfo("Real IMU data time shift vs. real gt: %f sec", shift_real)
        time_real_imu += shift_real
        zero_index = np.argmax(time_real_imu >= 0)
        # cut off the negative time part
        time_real_imu = time_real_imu[zero_index:] 
        yaw_real_imu = yaw_real_imu[zero_index:]

    plt.title('YAW angle comparison')
    plt.plot(time_sim_gt, yaw_sim_gt, label='simulated gt', linewidth=0.8, linestyle=':')
    plt.plot(time_real_gt, yaw_real_gt, label='real gt', linewidth=0.8, linestyle=':')
    plt.plot(time_sim_imu, yaw_sim_imu, label='simulated IMU',linewidth=0.8)
    plt.plot(time_real_imu, yaw_real_imu, label=real_imu_label, linewidth=0.8)
    
    plt.xlabel('Time [sec]')    
    plt.ylabel('YAW angle [rad]')
    plt.legend(loc='lower right')
    plt.grid()
    
    if external is False:
        plt.savefig(output_dir + time_suffix + '_imu_yaw.png', dpi=300)
        rospy.loginfo('Plotted yaw comparison saved in in:\n ---> ' + output_dir+time_suffix + '_imu_yaw.png')
    else:
        plt.show()

def plot_yaw_difference(time_sim_gt, time_sim_imu, time_real_gt, time_real_imu,
                        yaw_sim_gt, yaw_sim_imu, yaw_real_gt, yaw_real_imu,
                        time_shift=False, external=False, filter_peaks=False, window=519):
    
    """
    According to setting in the config file, this function computes yaw angle errors and plot them. 
    It returns also the errors for further post-processing.
    """
    if time_shift is True:
        shift_real = utils.compute_2_signals_shift(time_real_gt, time_real_imu, yaw_real_gt, yaw_real_imu)
        rospy.loginfo("Real IMU vs. real gt time shift: %f sec", shift_real)
        time_real_imu += shift_real
        zero_index = np.argmax(time_real_imu >= 0)
    
        # cut off the negative time part
        time_real_imu = time_real_imu[zero_index:] 
        yaw_real_imu = yaw_real_imu[zero_index:]

    time_diff_real, real_diff_raw = utils.compute_1l_difference(time_real_gt, time_real_imu, yaw_real_gt, yaw_real_imu,time_step=0.01, angle=True)
    time_diff_sim, sim_diff_raw = utils.compute_1l_difference(time_sim_gt, time_sim_imu, yaw_sim_gt, yaw_sim_imu, time_step=0.01, angle=True)
    
    # apply the median filter to remove the step changes caused by imperfect time matching
    # window must be odd number
    if filter_peaks is True:
        real_diff = medfilt(real_diff_raw,kernel_size=window)
        sim_diff = medfilt(sim_diff_raw,kernel_size=window)

    # plot the yaw error over time
    plt.figure('Yaw error')
    plt.plot(time_diff_real, real_diff, label=real_imu_label + ' vs. real gt' ,color='#d38a78')
    plt.plot(time_diff_sim, sim_diff, label='simulated IMU vs. simulated gt',color='#a638bc')
    plt.legend(loc='lower right')
    plt.grid()
    plt.xlabel('Time [sec]')
    plt.ylabel('Yaw error [rad]')

    if time_shift is True and filter_peaks is True:
        plt.title('Yaw error, time axis aligned\nsteep peaks filtered out by median filter')
    elif time_shift is True and filter_peaks is False:
        plt.title('Yaw error, time axis aligned')
    else:
        plt.title('Yaw error')

    if external is False: # do not save the figures when calling this function from different module
        if time_shift is True:    
            plt.savefig(output_dir+time_suffix+'_yaw_error_aligned_time.png')
            plt.savefig(output_dir+time_suffix+'_yaw_error_aligned_time.eps')
            rospy.loginfo('Plotted yaw error with aligned time saved in :\n ---> ' + output_dir+time_suffix + '_yaw_error_aligned_time.png')
            rospy.loginfo('Plotted yaw error with aligned time saved in :\n ---> ' + output_dir+time_suffix + '_yaw_error_aligned_time.eps')
        else:
            plt.savefig(output_dir+time_suffix+'_yaw_error.png')
            plt.savefig(output_dir+time_suffix+'_yaw_error.eps')
            rospy.loginfo('Plotted yaw error with aligned time saved in :\n ---> ' + output_dir+time_suffix + '_yaw_error.png')
            rospy.loginfo('Plotted yaw error with aligned time saved in :\n ---> ' + output_dir+time_suffix + '_yaw_error.eps')
    
    # Plot the yaw errros - difference between the real ans simulated yaw errors
    time_diff_diff, diff = utils.compute_1l_difference(time_diff_real, time_diff_sim, real_diff, sim_diff, time_step=0.01, abs=True, angle=True)    
    plt.figure('Difference between yaw errors')
    plt.plot(time_diff_diff, diff, label='difference', color='r')
    plt.xlabel('Time [sec]')
    plt.ylabel('yaw error_real - yaw error_sim')
    plt.grid()
    plt.title('Yaw errors difference')

    if external is False:
        plt.savefig(output_dir+time_suffix+'_yaw_errors_difference.png')
        rospy.loginfo('Plotted yaw errors difference saved in :\n ---> ' + output_dir+time_suffix + '_yaw_errors_difference.png')

    return sim_diff_raw, real_diff_raw

def plot_psd_yaw(yaw_sim_imu, yaw_real_imu, external=False):
    """
    Compute the Power spectral density function graph for the yaw angle IMU outputs (simulated and real) in log-scale.
    """
    plt.figure('psd_yaw')

    # assume that the sampling frequency was 100Hz for the simulation and 50Hz for the real measuremet
    f2, Pxx2, nperseg, _ = utils.compute_psd_welch(yaw_real_imu, 0.02)
    f1, Pxx1, nperseg, _ = utils.compute_psd_welch(yaw_sim_imu, 0.01)
    plt.plot(f1, Pxx1, label='simulated IMU yaw estimation')
    plt.plot(f2, Pxx2, label='real IMU yaw estimation')
    plt.title('Power Spectral Density functions - Welch\'s method\n simulated IMU vs. real IMU yaw angle estimation, nperseg: '+ str(nperseg))
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [rad^2/Hz]')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')  
    plt.grid(which='both')

    if external is False:
        plt.savefig(output_dir+time_suffix+'_yaw_psd.png')
        plt.savefig(output_dir+time_suffix+'_yaw_psd.eps')
        rospy.loginfo('Plotted PSD function of simulated YAW output was saved in :\n ---> ' + output_dir+time_suffix + '_yaw_psd.png')   
        rospy.loginfo('Plotted PSD function of simulated YAW output was saved in :\n ---> ' + output_dir+time_suffix + '_yaw_psd.eps')   
    else:
        plt.show()

def plot_psd_yaw_errors(yaw_sim_diff, yaw_real_diff, external=False):
    """
    Plot PSD of yaw angle errors signals into mutual graph in log-scale.
    """
    # Both yaw errors were computed with step 0.01 in "plot_yaw_difference" function
    f1, Pxx1, nperseg, l = utils.compute_psd_welch(yaw_sim_diff, 0.01)
    f2, Pxx2, nperseg, l = utils.compute_psd_welch(yaw_real_diff, 0.01)

    plt.figure('psd_errors')
    plt.title('Power Spectral Density function - Welch\'s method\n yaw angle error, nperseg: '+str(nperseg))
    plt.plot(f1, Pxx1, label='simulated IMU yaw error PSD')
    plt.plot(f2, Pxx2, label='real IMU yaw error PSD')
    plt.xlabel('frequency [Hz]')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel('PSD [rad^2/Hz]')
    plt.grid(which='both')
    plt.legend()

    if external is False:
        plt.savefig(output_dir+time_suffix+'_yaw_errors_psd.png')
        plt.savefig(output_dir+time_suffix+'_yaw_errors_psd.eps')
        rospy.loginfo('Plotted PSD function of yaw errors was saved in :\n ---> ' + output_dir+time_suffix + '_yaw_errors_psd.png')       
        rospy.loginfo('Plotted PSD function of yaw errors was saved in :\n ---> ' + output_dir+time_suffix + '_yaw_errors_psd.eps')       
    else:
        plt.show()

def data_comparison():
    """
    Main function, from which all data comparison functions are called.
    """

    if plot_trajectory is True:
        plot_trajectories(
            x_ref=real_position[:,1],\
            y_ref=real_position[:,2],\
            x_sim=simulated_position[:,1],\
            y_sim=simulated_position[:,2]
            )  

    time_sim = simulated_wheel_velocity[:,0] - simulated_wheel_velocity[0, 0]
    
    if plot_wheel_velocities is True:

        plot_wheel_velocities(
            time_sim = time_sim,\
            time_ref = real_wheel_velocities[:, 0],\
            vel_l_ref = real_wheel_velocities[:, 1],\
            vel_r_ref = real_wheel_velocities[:, 2],\
            vel_l_sim = simulated_wheel_velocity[:, 1],\
            vel_r_sim = simulated_wheel_velocity[:, 2]
            )

    if plot_wheel_velocities_diff is True:
        plot_velocity_difference(
            time_sim = time_sim,\
            time_ref = real_wheel_velocities[:, 0],\
            vel_l_ref = real_wheel_velocities[:, 1],\
            vel_r_ref = real_wheel_velocities[:, 2],\
            vel_l_sim = simulated_wheel_velocity[:, 1],\
            vel_r_sim = simulated_wheel_velocity[:, 2]
            )
            
    quaterions = np.array([simulated_imu[:,1], simulated_imu[:, 2], simulated_imu[:,3], simulated_imu[:,4]])
    yaw_sim_imu = utils.quaternions_to_yaw(quaterions)    
    
    quaterions = np.array([real_imu[:,1], real_imu[:,2],  real_imu[:,3],  real_imu[:,4]])    
    yaw_real_imu = utils.quaternions_to_yaw(quaterions)
    
    # compute the offset of RAW IMU data
    offset = np.abs(real_position[0,3] - yaw_real_imu[0])
    yaw_real_imu = [ utils.normalize_yaw_angle(yaw_real_imu[i] + offset) for i in range(0, np.size(yaw_real_imu)) ]

    if plot_imu_gt_yaw is True:
        plot_yaw_angle(
                time_sim_gt=utils.normalize_array(simulated_position[:,0]),\
                time_sim_imu = utils.normalize_array(simulated_imu[:, 0]),\
                time_real_gt=utils.normalize_array(real_position[:,0]),\
                time_real_imu = real_imu[:,0],\
                yaw_sim_gt = simulated_position[:,3],\
                yaw_sim_imu = yaw_sim_imu,\
                yaw_real_gt=utils.normalize_yaw_angle(real_position[:,3]),\
                yaw_real_imu=yaw_real_imu            
                )

    if plot_psd is True:
        plot_psd_yaw(yaw_sim_imu, yaw_real_imu)        
    
    if plot_imu_gt_yaw_error is True:

        sim_diff, real_diff = plot_yaw_difference(
                time_sim_gt=utils.normalize_array(simulated_position[:,0]),\
                time_sim_imu = utils.normalize_array(simulated_imu[:, 0]),\
                time_real_gt=utils.normalize_array(real_position[:,0]),\
                time_real_imu = real_imu[:,0],\
                yaw_sim_gt = simulated_position[:,3],\
                yaw_sim_imu = yaw_sim_imu,\
                yaw_real_gt=utils.normalize_yaw_angle(real_position[:,3]),\
                yaw_real_imu=yaw_real_imu,\
                time_shift=time_shift,\
                filter_peaks=filter_peaks,\
                window=median_filter_window_size
        )
        
        if plot_psd is True:
            plot_psd_yaw_errors(sim_diff, real_diff)

##############################################################################
def truncate_data_arrays():
    """
    Truncate the numpy arrays with captured data to the real size.
    Returns true, if the data were captures correctly, false otherwise.
    """
    global simulated_imu, simulated_position, simulated_wheel_velocity, joint_rear_left, joint_rear_right
    
    if simulated_position_idx == 0 or simulated_imu_idx == 0:
        return False
    if np.amax(np.shape(real_imu)) == 11:
        return False 

    velocity_start_index, velocity_stop_index, position_start_index = find_experiment_boundaries()
    simulated_position = simulated_position[position_start_index:simulated_position_idx, :]
    simulated_wheel_velocity = simulated_wheel_velocity[velocity_start_index:velocity_stop_index, :]
    imu_start_idx = utils.find_nearest_index(simulated_imu[:,0], value=start_time)
    imu_stop_idx = utils.find_nearest_index(simulated_imu[:,0], value=stop_time, start_idx=imu_start_idx)
    simulated_imu = simulated_imu[imu_start_idx:imu_stop_idx, :]
    joint_rear_left = joint_rear_left[:joint_rear_left_idx, :]
    joint_rear_right = joint_rear_right[:joint_rear_right_idx, :]
    return True
    
def save_captured_data_csv():
    """ 
    Save captured data into csv files for future post-processing. 
    """ 
    if (start_time == 0 and stop_time == 0):
        rospy.logerr("The start and stop of the experiment was not recognised, closing without any data savig and result comparison!")
        sys.exit(-1)
    
    file_name_position = output_dir+time_suffix+'_simulated_position.csv'
    file_name_simulated_imu = output_dir + time_suffix +'_simulated_imu.csv'
    file_name_real_imu = output_dir+ time_suffix + '_real_imu_cropped.csv'
    file_name_wheel_vel = output_dir+ time_suffix + '_simulated_wheel_vel.csv'
   
    # file_name_rear_joints_left = output_dir+time_suffix+'_rear_joints_left_command.csv'
    # file_name_rear_joints_right = output_dir+ time_suffix+'_rear_joints_right_command.csv'
    # file_name_timing = output_dir+time_suffix+'timing_parameters.csv'
        
    rospy.loginfo("The captured data were saved in the csv files:")

    df = pd.DataFrame(simulated_position)
    df.to_csv(path_or_buf=file_name_position, header=['time','x','y','yaw'], index=False, sep=';')
    rospy.loginfo("---> %s", file_name_position)
        
    df = pd.DataFrame(simulated_imu)
    df.to_csv(path_or_buf=file_name_simulated_imu, header=['time','qx', 'qy', 'qz','qw', 'accel_x','accel_y','accel_z','ang_vel_x', 'ang_vel_y','ang_vel_z'], index=False, sep=';')
    rospy.loginfo("---> %s", file_name_simulated_imu)
    
    df = pd.DataFrame(real_imu)
    df.to_csv(path_or_buf=file_name_real_imu, header=['time','qx', 'qy', 'qz','qw', 'accel_x','accel_y','accel_z','ang_vel_x', 'ang_vel_y','ang_vel_z'], index=False, sep=';')
    rospy.loginfo("---> %s", file_name_real_imu)

    df = pd.DataFrame(simulated_wheel_velocity)
    df.to_csv(path_or_buf=file_name_wheel_vel, header=['time','velocity_left','velocity_right'],index=False, sep=';')    
    rospy.loginfo("---> %s", file_name_wheel_vel)
    
    # df = pd.DataFrame(joint_rear_left)
    # df.to_csv(path_or_buf=file_name_rear_joints_left, header=['time','command_left'], index=False, sep=';')

    # df = pd.DataFrame(joint_rear_right)
    # df.to_csv(path_or_buf=file_name_rear_joints_right, header=['time','command_right'], index=False, sep=';')    

def exit_gracefully(sig=None, stackframe=None):
    """
    Stop capturing data, save result into csv file and exit this script.
    """
    global kill_now
    kill_now = True
    rospy.sleep(1)
    
    if truncate_data_arrays() is True: # truncate the data to fit to the experiment boundaries
        save_captured_data_csv()    
        data_comparison()
    else: 
        rospy.logerr("Result comparison was not able to capture imu or simulated_gt data, script is closing without success!")
        
    plt.close('all')
    rospy.loginfo("Result comparison script closing, good bye!")
    rospy.signal_shutdown('Shuting down all ROS nodes!')
        
##############################################################################
# Callbacks

def odometry_callback(msg, output=False):
    """ 
    The (ground truth) position of the robot model is published as odometry using P3D plugin. 
    """
    global simulated_position, simulated_position_idx
    
    if kill_now is False and collect_data is True:   
        rostime = msg.header.stamp
        t = rostime.to_sec()
        qx = msg.pose.pose.orientation.x
        qy = msg.pose.pose.orientation.y
        qz = msg.pose.pose.orientation.z
        qw = msg.pose.pose.orientation.w
        quaternions = np.array([[qx], [qy], [qz], [qw]])
        yaw = utils.quaternions_to_yaw(quaternions, axes='sxyz')
        try:
            simulated_position[simulated_position_idx, 0] = t
            simulated_position[simulated_position_idx, 1] = msg.pose.pose.position.x
            simulated_position[simulated_position_idx, 2] = msg.pose.pose.position.y
            simulated_position[simulated_position_idx, 3] = yaw
            simulated_position_idx += 1    

        except IndexError:
            rospy.loginfo("The maximal number of captured item exceeded!")
            exit_gracefully()
       
        if output:
            rospy.loginfo("Simulated position at time: %.3fsec: [x: %.3fm, y:%.3fm, yaw:%4frad]",
            t, msg.pose[idx].position.x, msg.pose[idx].position.x, yaw[0])       
          
def imu_callback(msg, output=False):
    global simulated_imu, simulated_imu_idx
    if (not kill_now and collect_data):
        t = utils.get_time_in_seconds(utils.get_time_in_nanoseconds(msg.header.stamp.secs, msg.header.stamp.nsecs))
        
        qx = msg.orientation.x
        qy = msg.orientation.y
        qz = msg.orientation.z
        qw = msg.orientation.w
        acc_x = msg.linear_acceleration.x
        acc_y = msg.linear_acceleration.y
        acc_z = msg.linear_acceleration.z
        gyro_x = msg.angular_velocity.x
        gyro_y = msg.angular_velocity.y
        gyro_z = msg.angular_velocity.z

        try:
            simulated_imu[simulated_imu_idx, 0] = t
            simulated_imu[simulated_imu_idx, 1] = qx
            simulated_imu[simulated_imu_idx, 2] = qy
            simulated_imu[simulated_imu_idx, 3] = qz
            simulated_imu[simulated_imu_idx, 4] = qw
            simulated_imu[simulated_imu_idx, 5] = acc_x
            simulated_imu[simulated_imu_idx, 6] = acc_y
            simulated_imu[simulated_imu_idx, 7] = acc_z
            simulated_imu[simulated_imu_idx, 8] = gyro_x
            simulated_imu[simulated_imu_idx, 9] = gyro_y
            simulated_imu[simulated_imu_idx, 10] = gyro_z
            
            simulated_imu_idx += 1
        except IndexError:
            rospy.loginfo("The maximal number of captured item exceeded!")
            exit_gracefully()
        
        if output:
            quaternions = np.array([[qx], [qy], [qz], [qw]])
            rospy.loginfo("IMU yaw angle:%f", utils.quaternions_to_yaw(quaternions))
            rospy.loginfo("Simulated imu: %.3fsec: \n linear:[x:%.3f, y:%3f, z:%.3f]m/s^2\n angular:[x:%.3f, y:%3f, z:%.3f]rad/sec",
                utils.get_time_in_seconds(t), acc_x,acc_y,acc_z, gyro_x, gyro_y,gyro_z)

def wheel_velocity_command_callback(msg):
    """ 
    Capture the moment, when the wheel velocities start to be produced.
    """
    global start_time, stop_time
    if kill_now is False:
        stop_time = rospy.get_rostime().to_sec()

        if start_time == 0: # save the start time of experiment
            t = rospy.get_rostime()
            start_time = t.to_sec()
            rospy.loginfo("ROS time: %f detected as experiment start time", start_time)

            # check if the IMU topic is active, if not, stop the script immediately
            if simulated_imu_idx == 0:
                rospy.logerr("No ROS messages detected! Result comparison script closing!")
                exit_gracefully()

def wheel_velocity_callback(msg, output=False):
    """
    Instantaneous wheel velocity and wheel position as published by joint_state_controller"
    """
    global simulated_wheel_velocity, simulated_wheel_velocity_idx, start_time
    if (not kill_now and collect_data):
        t = utils.get_time_in_seconds(utils.get_time_in_nanoseconds(msg.header.stamp.secs, msg.header.stamp.nsecs))
        velocity_left = msg.velocity[0]
        velocity_right = msg.velocity[1]
        try:
            simulated_wheel_velocity[simulated_wheel_velocity_idx, 0] = t
            simulated_wheel_velocity[simulated_wheel_velocity_idx, 1] = velocity_left
            simulated_wheel_velocity[simulated_wheel_velocity_idx, 2] = velocity_right
            simulated_wheel_velocity_idx += 1
        except IndexError:
            rospy.loginfo("The maximal number of captured item exceeded!")
            exit_gracefully()

        if output:
            rospy.loginfo("Simulated wheel velocities: %.3fsec [l:%.3f, r: %.3f]", t, velocity_left, velocity_right)

def path_follower_command_callback(req):
    """
    Handle the trigger request sent from path follower script.
    Start/stop capturing the data when received.
    """
    global collect_data
    
    if start_time == 0:
        rospy.loginfo("I received a trigger request from path_follower script, I'm starting to gather the data!")
    else:
        rospy.loginfo("I received a stop request, I stop data capture!")
    
    if (req.data is True): # either start data collection, or end the script

        if use_hector_imu is True:
            rospy.sleep(0.1)
            hector_imu_reset_yaw_model() # set the IMU's yaw model according to the loaded configuration      
        collect_data = True

    elif(req.data is not True):
        collect_data = False
        exit_gracefully()
    
    return SetBoolResponse(True, 'Command successfully received!')

def path_follower_start_stop_time_callback(req):
    """
    Save the start and stop times of the experiment as saved in the provided raw data.
    Saving the start and stop times is necessary to load raw IMU data correctly.
    """
    global raw_data_start_rostime, raw_data_stop_rostime, real_imu, time_suffix
    
    raw_data_start_rostime = req.start_rostime
    raw_data_stop_rostime = req.stop_rostime 
    time_suffix = req.time_suffix
    if time_suffix == '':
        time_suffix = time.strftime("%Y%m%d-%H%M%S")
    rospy.loginfo("I received start and stop rostimes of the experiment!\nstart_time_secs:%f, stop_time_secs:%f", raw_data_start_rostime.secs, raw_data_stop_rostime.secs)
    rospy.loginfo("Duration: %f", (raw_data_stop_rostime - raw_data_start_rostime).secs )

    if raw_data_start_rostime.secs != 0 and raw_data_stop_rostime.secs != 0:
        try:
            time, acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z, qx, qy, qz, qw = utils.load_data_imu(imu_raw_data_csv_file, raw_data_start_rostime, raw_data_stop_rostime) 
            real_imu = np.vstack((time, qx, qy, qz, qw, acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z)).T
        except AssertionError:
            rospy.logerr("Error, no raw imu data were loaded!\n Closing the result_comparison script.")
            exit_gracefully()
    
    return StartStopTimeResponse(True)

def joint_rear_right_callback(msg, output=False): 
    global joint_rear_right, joint_rear_right_idx
    if kill_now is not True and collect_data is True:
        if output is True:
            rospy.loginfo("Joint rear right callback!")
        t = msg.header.stamp.secs + utils.get_time_in_seconds(msg.header.stamp.nsecs)
        command = msg.command
        try:
            joint_rear_right[joint_rear_right_idx, 0] = t
            joint_rear_right[joint_rear_right_idx, 1] = command
            joint_rear_right_idx += 1
        except IndexError:
            rospy.loginfo("The maximal number of captured item exceeded!")
            exit_gracefully()

def joint_rear_left_callback(msg, output=False): 
    global joint_rear_left, joint_rear_left_idx
    if kill_now is not True and collect_data is True:
        # if output is True:
        #     rospy.loginfo("Joint rear left callback!")     
        t = msg.header.stamp.secs + utils.get_time_in_seconds(msg.header.stamp.nsecs)
        command = msg.command
        
        try:
            joint_rear_left[joint_rear_left_idx, 0] = t
            joint_rear_left[joint_rear_left_idx, 1] = command
            joint_rear_left_idx += 1

        except IndexError:
            rospy.loginfo("The maximal number of captured item exceeded!")
            exit_gracefully()

def hector_imu_current_bias_callback(msg):
    global hector_bias
    
    if collect_data is False:
        qx = msg.orientation.x
        qy = msg.orientation.y
        qz = msg.orientation.z
        qw = msg.orientation.w
        quaterions = np.array([[qx], [qy], [qz], [qw]])
        hector_bias = utils.quaternions_to_yaw(quaterions)[0]
    # rospy.loginfo("Current bias: %f deg", hector_bias)

def hector_imu_reset_yaw_model():
    """
    Since there is no ROS service to reseting the yaw noise (its separate noise model) in Hector IMU ros_gazebo plugin,
    reset the bias value manualy in the begining of the experiment.
    """   
    rospy.loginfo("current Hector imu bias: %f", hector_bias)
    params = {'offset':-1*hector_bias,'drift':drift_std, 'drift_frequency':drift_frequency, 'gaussian_noise':white_noise_std}
    hector_imu_config_yaw_model(params)

def hector_imu_config_yaw_model(params):
    """
    General function to set the yaw model in Hector IMU model.
    """
    client = dynamic_reconfigure.client.Client('/imu/yaw')
    config = client.update_configuration(params)
    rospy.loginfo("Hector IMU plugin configured, yaw model setting:\n%s\n", config)

##############################################################################
def init_ros():    
    # rospy.Subscriber(gt_topic, Odometry, odometry_callback)
    rospy.Subscriber(imu_topic, Imu, imu_callback)
    rospy.Subscriber(joint_states_topic, JointState, wheel_velocity_callback)
    
    # use it only as a trigger to start experiment:
    rospy.Subscriber(wheel_velocity_command_topic, Float64, wheel_velocity_command_callback)
    
    # the wheel velocity controller data:
    rospy.Subscriber(rear_wheel_velocity_left_topic, JointControllerState, joint_rear_left_callback)
    rospy.Subscriber(rear_wheel_velocity_right_topic, JointControllerState, joint_rear_right_callback)
    
    # rospy.Subscriber("/gazebo/link_states", LinkStates, odometry_callback)
    rospy.Subscriber(gt_topic, Odometry, odometry_callback)
    rospy.Service('collect_data', SetBool, path_follower_command_callback)
    
    # rospy.spin()
    while not rospy.is_shutdown():
        continue

def load_parameters(ini_file):
    """Load the parameters from provide ini_file."""

    global array_size, plot_trajectory, plot_wheel_velocities, \
    plot_wheel_velocities_diff, plot_imu_gt_yaw, plot_imu_gt_yaw_error,\
    real_imu_label, imu_topic, joint_states_topic, \
    gt_topic, wheel_velocity_command_topic, rear_wheel_velocity_left_topic,\
    rear_wheel_velocity_right_topic, output_dir, imu_raw_data_csv_file, \
    filter_peaks, median_filter_window_size, use_hector_imu, time_shift, drift_std,\
    drift_frequency, white_noise_std, plot_psd

    config = ConfigParser.SafeConfigParser()

    if os.path.isfile(ini_file):
        config.read(ini_file)
    else:
        rospy.logerr("Error, the config file: %s not found!", ini_file)
        return False        
    try:
        rospy.loginfo('Loading the parameters from config file: %s \n--------------------------------------------------', ini_file)
        plot_trajectory = config.getboolean('result_comparison','plot_trajectory')
        rospy.loginfo("plot_trajectory set to: %d", plot_trajectory)      
        plot_wheel_velocities = config.getboolean('result_comparison','plot_wheel_velocities')
        rospy.loginfo("plot_wheel_velocities set to: %d", plot_wheel_velocities)
        plot_wheel_velocities_diff = config.getboolean('result_comparison','plot_wheel_velocities_diff')
        rospy.loginfo("plot_wheel_velocities_diff set to: %d", plot_wheel_velocities_diff)
        plot_imu_gt_yaw = config.getboolean('result_comparison','plot_imu_gt_yaw')
        rospy.loginfo("plot_imu_gt_yaw set to: %d", plot_imu_gt_yaw)
        plot_imu_gt_yaw_error = config.getboolean('result_comparison','plot_imu_gt_yaw_error')

        filter_peaks= config.getboolean('result_comparison', 'filter_peaks')
        rospy.loginfo("filter_peaks set to: %d", filter_peaks)
        plot_psd = config.getboolean('result_comparison', 'plot_psd')
        rospy.loginfo("plot_psd set to: %d", plot_psd)
        median_filter_window_size = config.getint('result_comparison', 'median_filter_window_size')
        rospy.loginfo("median_filter_window_size set to: %d", median_filter_window_size)
        time_shift = config.getboolean('result_comparison','time_shift')
        rospy.loginfo("time_shift set to: %d", time_shift)
        rospy.loginfo("plot_imu_gt_yaw_error set to: %d", plot_imu_gt_yaw_error)
        array_size = config.getint('result_comparison','array_size')
        rospy.loginfo("array_size set to: %f", array_size)
        real_imu_label = config.get('result_comparison', 'real_imu_label')
        rospy.loginfo("real_imu_label set to:%s", real_imu_label)
        
        imu_raw_data_csv_file = config.get('common', 'imu_raw_data_csv_file')
        rospy.loginfo("imu_raw_data_csv_file set to:%s", imu_raw_data_csv_file)
        output_dir = config.get('common', 'output_dir')
        rospy.loginfo("output_dir set to:%s", output_dir)

        imu_topic = config.get('result_comparison', 'imu_topic')
        rospy.loginfo("imu_topic set to:%s", imu_topic)
        gt_topic = config.get('result_comparison','gt_topic')
        rospy.loginfo("gt_topic set to:%s", gt_topic)
        joint_states_topic = config.get('result_comparison', 'joint_states_topic')
        rospy.loginfo("joint_states_topic set to:%s", joint_states_topic)
        wheel_velocity_command_topic = config.get('result_comparison', 'wheel_velocity_command_topic')
        rospy.loginfo("wheel_velocity_command_topic set to:%s", wheel_velocity_command_topic)
        rear_wheel_velocity_left_topic = config.get('result_comparison', 'rear_wheel_velocity_left_topic')
        rospy.loginfo("rear_wheel_velocity_left_topic set to:%s", rear_wheel_velocity_left_topic)
        rear_wheel_velocity_right_topic = config.get('result_comparison', 'rear_wheel_velocity_right_topic')
        rospy.loginfo("rear_wheel_velocity_right_topic set to:%s", rear_wheel_velocity_right_topic)

        use_hector_imu = config.getboolean('imu_model', 'use_hector_imu')
        rospy.loginfo("use_hector_imu set to:%d",use_hector_imu)
        white_noise_std = config.getfloat('imu_model', 'white_noise_std')
        rospy.loginfo("white_noise_std set to:%d",white_noise_std)

        drift_std = config.getfloat('imu_model', 'drift_std')
        rospy.loginfo("drift_std set to:%f",drift_std)

        drift_frequency = config.getfloat('imu_model', 'drift_frequency')
        rospy.loginfo("drift_std set to:%f", drift_frequency)
        rospy.loginfo('--------------------------------------------------\n')
    except ConfigParser.NoSectionError as e:
        rospy.logerr("NoSectionError when parsing the config file: %s", ini_file)
        rospy.logerr(e)
        return False
    except ConfigParser.NoOptionError as e:
        rospy.logerr("NoOptionError when parsing the config file: %s", ini_file)
        rospy.logerr(e)
        return False
    except ValueError as e:
        rospy.logerr("Wrong value given in config file: %s",ini_file)
        rospy.logerr(e)
        return False
    return True

def main(ini_file): 
    global real_imu, real_wheel_velocities, real_position
   
    rospy.init_node('result_comparison', anonymous=False, disable_signals=True)
    rospy.Service('/start_stop_time', StartStopTime, path_follower_start_stop_time_callback)
    rospy.loginfo("The result_comparison script started!")
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
            
    if load_parameters(ini_file) is not False:            
        
        try:
            if use_hector_imu is True:
                # if the hector imu plugin is used, listen to the current bias drift value
                rospy.Subscriber("/imu/bias", Imu, hector_imu_current_bias_callback)
    
            t, x, y, yaw, velocity_left, velocity_right = utils.load_trajectory_data(reference_pose_file= output_dir + 'trajectory_data.csv')    
            lenght = np.amax(np.shape(t))
            real_position.resize((lenght, np.amax(np.shape(real_position))))
            real_position = np.vstack((t, x, y, yaw)).T
            real_wheel_velocities.resize((np.shape(t)))
            real_wheel_velocities = np.vstack((t, velocity_left, velocity_right)).T
            
            # init ros and gether data from gazebo
            init_ros() 
            rospy.loginfo("Waiting for path follower trigger event...")
                                                    
        except (IOError, TypeError, NameError) as e:
            rospy.loginfo(e)
            return 0
    
        
    else:
        rospy.logerr("There was an error when loading the confing, result_comparison closing.")

if __name__ == '__main__':
    
    try:
        if len(sys.argv) > 1:
            main(sys.argv[1])
        else:
            raise AssertionError
            
    except AssertionError:
        print("Result_comparison error: no config file given!")
    
    
