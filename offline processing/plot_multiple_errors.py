#!/usr/bin/env python 

from matplotlib import pyplot as plt 
import rospy
import sys
import numpy as np
import pandas as pd
from scipy.signal import medfilt
sys.path.insert(0, '/home/david/catkin_ws/src/imu_simulation_framework/scripts/')
import utils

import os
simulated_log_dir = '/home/david/simulation_log/'

parameters= '$\sigma_{drift}$=1.118 rad, $f_d$=0.001 Hz'

array = []

"""
Offline plotting of multiple yaw errors, used to create the graphs for master's thesis.
David Cesenek, cesendav@fel.cvut.cz
"""

# 1
file = simulated_log_dir + 'sim1.csv'
a = pd.read_csv(file, delimiter=';')
aa = a.values
array.append(aa)

# 2
file = simulated_log_dir + 'sim2.csv'
b = pd.read_csv(file, delimiter=';')
bb = b.values
array.append(bb)

# 3
file = simulated_log_dir + 'sim3.csv'
c = pd.read_csv(file, delimiter=';')
cc = c.values
array.append(cc)

# # 4
# file = simulated_log_dir + 'sim4.csv'
# d = pd.read_csv(file, delimiter=';')
# dd = d.values
# array.append(dd)

# 5
# file = simulated_log_dir + 'sim5.csv'
# e = pd.read_csv(file, delimiter=';')
# ee = e.values
# array.append(ee)

# real
file = simulated_log_dir + 'real.csv'
f = pd.read_csv(file, delimiter=';')
ff = f.values
array.append(ff)


# array = [aa, bb, cc, dd, ee, ff]
# array = [aa, bb, cc, dd]
for i, value in enumerate(array):
    value[:,1] = medfilt(value[:,1], kernel_size=591)    

plt.figure("Yaw errors - trajectory A")

plt.plot(ff[:,0], ff[:,1],label='real BNO055 vs. real gt', linewidth=1.5, color='#d38a78')

plt.plot(aa[:,0], aa[:,1],label='simulated IMU vs. simulated gt: experiment 1', linewidth=0.8)
plt.plot(bb[:,0], bb[:,1],label='simulated IMU vs. simulated gt: experiment 2', linewidth=0.8)
plt.plot(cc[:,0], cc[:,1],label='simulated IMU vs. simulated gt: experiment 3', linewidth=0.8)
# plt.plot(dd[:,0], dd[:,1],label='simulated IMU vs. simulated gt: experiment 4', linewidth=0.8)
# plt.plot(ee[:,0], ee[:,1],label='simulated IMU vs. simulated gt: experiment 5', linewidth=0.8)

plt.title('Yaw errors\n'+parameters)
plt.xlabel('Time [sec]')
plt.ylabel('Yaw error [rad]')
plt.legend(loc='upper left')
plt.grid()
plt.show()

# PLOT mutual PSD function graph
f6, Pxx6, nperseg, l = utils.compute_psd_welch(ff[:,1], 0.01)

f1, Pxx1, nperseg, l = utils.compute_psd_welch(aa[:,1], 0.01, nperseg)
f2, Pxx2, nperseg, l = utils.compute_psd_welch(bb[:,1], 0.01, nperseg)
f3, Pxx3, nperseg, l = utils.compute_psd_welch(cc[:,1], 0.01, nperseg)
# f4, Pxx4, nperseg, l = utils.compute_psd_welch(dd[:,1], 0.01, nperseg)
# f5, Pxx5, nperseg, l = utils.compute_psd_welch(ee[:,1], 0.01, nperseg)

plt.figure('psd_errors')

plt.title('Power Spectral Density function - Welch\'s method\n IMU yaw angle errors, nperseg: '+str(nperseg)+', ' + parameters)

plt.plot(f6, Pxx6, label='real BNO055 IMU yaw error PSD',linewidth=1.5, color='#d38a78')

plt.plot(f1, Pxx1, label='simulated IMU yaw error PSD: experiment 1',linewidth=0.8)
plt.plot(f2, Pxx2, label='simulated IMU yaw error PSD: experiment 2',linewidth=0.8)
plt.plot(f3, Pxx3, label='simulated IMU yaw error PSD: experiment 3',linewidth=0.8)
# plt.plot(f4, Pxx4, label='simulated IMU yaw error PSD: experiment 4',linewidth=0.8)
# plt.plot(f5, Pxx5, label='simulated IMU yaw error PSD: experiment 5',linewidth=0.8)
plt.xlabel('frequency [Hz]')
plt.xscale('log')
plt.yscale('log')
plt.ylabel('PSD [rad^2/Hz]')
plt.grid(which='both')
plt.legend(loc='lower left')
plt.show()
