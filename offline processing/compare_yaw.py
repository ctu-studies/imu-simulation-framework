#!/usr/bin/env python
import pandas as pd
import sys
import numpy as np
sys.path.insert(0, '/home/david/catkin_ws/src/imu_simulation_framework/scripts/')
import utils
from matplotlib import pyplot as plt 
from result_comparison import plot_yaw_difference
file_real = '/home/david/raw_data/imu_raw_data.csv'
file_ref = '/home/david/raw_data/gps_raw_data.csv'
from scipy.signal import medfilt

"""
Offline ploting of real IMU data with respect to GPS reference data.
"""

# reference trajectory data (GPS)
df = pd.read_csv(file_ref, delimiter=';')
quaternions = np.array([df['x.1'].values, df['y.1'].values, df['z.1'].values, df['w'].values ])
yaw_ref = utils.quaternions_to_yaw(quaternions)
time_ref_sec = df['secs'].values
time_ref_nsec = df['nsecs'].values
time_ref = utils.get_time_in_seconds(utils.get_time_in_nanoseconds(time_ref_sec, time_ref_nsec))
time_ref = time_ref - time_ref[0]

# real IMU - quaternions
real = pd.read_csv(file_real, delimiter=';')

time_real_sec = real['secs'].values
time_real_nsec = real['nsecs'].values
time_real = utils.get_time_in_seconds(utils.get_time_in_nanoseconds(time_real_sec, time_real_nsec))
time_real = time_real - time_real[0]
time_real = time_real

qx_r = real['x'].values
qy_r = real['y'].values
qz_r = real['z'].values
qw_r = real['w'].values
q_real = np.array([qx_r, qy_r, qz_r, qw_r])
yaw_real = utils.quaternions_to_yaw(q_real)

yaw_real = utils.normalize_yaw_angle(yaw_real)

offset = np.abs(yaw_real[0] - yaw_ref[0])
yaw_real = [ utils.normalize_yaw_angle(yaw_real[i] - offset) for i in range(0, np.size(yaw_real)) ]
# start_index_imu = 0
# stop_index_imu = 70000
# stop_index_gt = 3000
# plt.figure(1)
# plt.plot(time_ref[:],  yaw_ref[:],  label='reference(GPS)', linewidth=0.5)
# plt.plot(time_real[:], yaw_real[:], label='real IMU', linewidth=1)
# plt.legend()
# plt.xlabel('Time [secs]')
# plt.ylabel('Yaw [rad]')
# plt.show()


# #############################33
time_diff, real_diff = utils.compute_1l_difference(time_ref,time_real,yaw_ref,yaw_real,time_step=0.001, angle=True)
    
real_diff = medfilt(real_diff,kernel_size=519)

# plot the yaw error over time
# plt.figure('Yaw error')
# plt.plot(time_diff, real_diff, label=' real imu vs. real gt' ,color='#d38a78')
# plt.legend(loc='lower right')
# plt.grid()
# plt.xlabel('Time [sec]')
# plt.ylabel('Yaw error [rad]')
# plt.show()

f,Pxx, nperseg,l =utils.compute_psd_welch(real_diff,0.001)
print('nperseg:',nperseg, 'l',l)
plt.figure(3)
plt.plot(f,Pxx)
plt.xlabel('frequency [Hz]')
plt.xscale('log')
plt.yscale('log')
plt.ylabel('PSD [rad^2/Hz]')
plt.grid(which='both')
plt.legend()

plt.show()


